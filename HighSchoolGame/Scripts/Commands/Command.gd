extends Node


export(bool) var active : bool = false

export(NodePath) var targetNode

onready var target : Spatial = get_node(targetNode)

func IsDone() -> bool:
	return not active

func Execute():
	active = true

func _process(delta):
	if not active:
		return
	
	UpdateTask(delta)

func UpdateTask(delta):
	pass
