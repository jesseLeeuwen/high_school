extends "res://Scripts/Commands/Command.gd"

export(float) var MaxSpeed = 5
export(float) var Acceleration = 18

var velocity : Vector3
export var targetPosition : Vector3

func UpdateTask(delta):
	
	var current = target.translation;
	var desiredVelocity = Truncate( targetPosition - current, MaxSpeed)
	var steering = Truncate( desiredVelocity - velocity, Acceleration )	
	velocity += steering * delta
	target.translation += velocity * delta

func Truncate( vec : Vector3, length : float) -> Vector3:
	if vec.length_squared() > length * length:
		return vec.normalized() * length
		
	return vec
