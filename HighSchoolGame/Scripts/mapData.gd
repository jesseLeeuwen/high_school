extends Node

export( float ) var gridCellSize = 2
export( Vector2 ) var gridSize = Vector2(10, 10)

export( Array ) var data = []

class CellData:
	var tileType : int = 0
	var actor : Node = null
	var objects = [null]

func _ready():
	data.resize( floor(gridSize.x * gridSize.y) )
	for i in range( len( data )):
		data[i] = CellData.new()

# returns: index of given position
func VectorToGrid( position : Vector2 ) -> Vector2:
	return ((position + gridSize * 0.5 * gridCellSize ) / gridCellSize).floor()
	
func GridToVector( gridPosition : Vector2 ) -> Vector2:
	return (gridPosition * gridCellSize).floor() - gridSize * 0.5 * gridCellSize

func Get( index : Vector2 ) -> CellData:
	return data[index.x + (index.y * gridSize.x)]

func GetActor( index : Vector2 ) -> CellData:
	return data[index.x + (index.y * gridSize.x)].actor

func GetObjects( index : Vector2 ) -> CellData:
	return data[index.x + (index.y * gridSize.x)].objects

func SetTile( index : Vector2,  value : int) -> void:
	data[index.x + (index.y * gridSize.x)].tileType = value

func SetActor( index : Vector2,  value : Node) -> void:
	data[index.x + (index.y * gridSize.x)].actor = value
