extends Spatial

export( Vector2 ) var index = Vector2(0,0)

func _ready():
	index = MapData.VectorToGrid( Vector2( translation.x, translation.z ) )
	MapData.SetActor( index, self )
