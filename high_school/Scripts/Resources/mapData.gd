extends Resource
class_name MapData

export(Array, Array, int) var data = []

func SetNodeValue(x: int, y : int, type : int):
	data[x][y] = type
	
func GetNodeValue(x: int, y: int) -> int:
	return data[x][y]